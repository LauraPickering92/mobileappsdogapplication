//
//  NewNoteViewController.swift
//  MobileAppsDogApplication
//
//  Created by Laura PIckering on 27/02/2020.
//  Copyright © 2020 Laura PIckering. All rights reserved.
//

import UIKit
import CoreData

class NewNoteViewController: UIViewController {
    
    @IBOutlet var taskName: UITextField!
    
    override func viewDidLoad() {
    }
    
    @IBAction func saveTask(_ sender: Any) {
        createTask {(done) in
            
            if done{
                navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }
            else {
                print ("Data not saved. Try again ")
            }
        }
    }
    
    func createTask(completion: (_ finished: Bool) -> ()){
     
        guard let managedContext = appDelegate?.persistentContainer.viewContext else
        {
            return
        }
        
        let task = Task(context: managedContext)
        
        task.taskTitle = taskName.text;
        task.taskStatus = false
        
        do {
        try managedContext.save()
        print("Task Details Saved")
        completion(true)
        }
        catch {
        print("Failed to save data: ", error.localizedDescription)
        completion(false)
        }
    }
}
