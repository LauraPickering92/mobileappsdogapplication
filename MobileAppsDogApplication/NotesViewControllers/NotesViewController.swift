//
//  NotesViewController.swift
//  MobileAppsDogApplication
//
//  Created by Laura PIckering on 27/02/2020.
//  Copyright © 2020 Laura PIckering. All rights reserved.
//

import UIKit
import CoreData

let appDelegate = UIApplication.shared.delegate as? AppDelegate

class NotesViewController: UIViewController {
    
    var cellid: String = "CellId"
    var tasksArray = [Task]()
    
    @IBOutlet var tvList: UITableView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        callDelegates()
    }
    
    func callDelegates(){
        tvList.delegate = self
        tvList.dataSource = self
        tvList.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        retrieveData
        { (done) in
            if done
            {
                if tasksArray.count > 0
                {
                    tvList.isHidden = false
                }
                else
                {
                    tvList.isHidden = true
                }
            }
        }
        
        fetchData()
        tvList.reloadData()
        
    }
    
    func fetchData(){
        retrieveData { (done) in
            if done {
                if tasksArray.count > 0{
                    tvList.isHidden = false
                }
                else {
                    tvList.isHidden = true
                }
            }
        }
    }
}

extension NotesViewController: UITableViewDataSource,
    UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tasksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for:indexPath) as! PrototypeCellController
        
        let task = tasksArray[indexPath.row]
        
        cell.taskLbl.text = task.taskTitle
        
        if task.taskStatus == true{
            cell.backgroundColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
            cell.taskLbl.textColor = UIColor.black
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath:
    IndexPath) -> Bool {
     return true
     }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt
    indexPath: IndexPath) -> UITableViewCell.EditingStyle {
     return .none
     }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt
        indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title:
        "Delete"){(action, indexPath) in self.deleData(indexPath: indexPath)
            
            self.fetchData()
            
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        let taskStatusAction = UITableViewRowAction(style: .normal, title:
        "Completed"){(action, IndexPath) in self.updateTaskStatus(indexPath:
            indexPath)
            
            self.fetchData()
            
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        
        taskStatusAction.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        
        deleteAction.backgroundColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
        
        return [deleteAction, taskStatusAction]
    }
}

extension NotesViewController {
    
    func retrieveData(completion: (_ complete: Bool) -> ()){
        
        guard let manageContext =
            appDelegate?.persistentContainer.viewContext else
        {
            return
        }
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName:
            "Task")
        
        do
        {
            tasksArray = try manageContext.fetch(request) as! [Task]
            print("Task details retrieved successfully")
            completion(true)
        }
        catch
        {
            print("Unable to retrieve data: ", error)
            completion(false)
        }
    }
    
    func deleData(indexPath: IndexPath){
        
        guard let managedContext =
        appDelegate?.persistentContainer.viewContext else
        {
            return
        }
        
        managedContext.delete(tasksArray[indexPath.row])
        
        do {
        try managedContext.save()
        print("Task Details Deleted")
  
        }
        catch {
         print("Failed to delete task details: ",
        error.localizedDescription)

         }
    }
    
    func updateTaskStatus(indexPath: IndexPath){
        
        guard let managedContext =
        appDelegate?.persistentContainer.viewContext else
        {
            return
        }
        
        let task = tasksArray[indexPath.row]
        
        if task.taskStatus == true{
        task.taskStatus = false
        }
        else {
        task.taskStatus = true
        }
        
        do {
        try managedContext.save()
        print("Task Status Updated")

        }
        catch {
         print("Failed to update task statuss: ",
        error.localizedDescription)

         }
    }
}
