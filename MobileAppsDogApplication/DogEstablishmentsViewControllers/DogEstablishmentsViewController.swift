//
//  DogEstablishmentsViewController.swift
//  MobileAppsDogApplication
//
//  Created by Laura PIckering on 19/03/2020.
//  Copyright © 2020 Laura PIckering. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class DogEstablishmentsViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet var textFieldForAddress: UITextField!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var getDirectionsButton: UIButton!
    
    // managing the users location
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        mapView.delegate = self
        
        self.CreatePin(title: "Pets at home: chesterfield", subtitle: "Supplies for your pets", lat: 53.233663, long: -1.429585)
        self.CreatePin(title: "Vets for pets: Littmore", subtitle: "A vet clinic", lat: 53.257050, long: -1.445947)
        self.CreatePin(title: "Pets at home: Dronfield", subtitle: "A vet clinic nearby", lat: 53.305332, long: -1.475062)
        self.CreatePin(title: "The Rectorty: Chesterfield", subtitle: "A dog friendly resturant", lat: 53.236190, long: -1.424397)
        self.CreatePin(title: "The Rectorty: Chesterfield", subtitle: "A dog friendly resturant", lat: 53.236190, long: -1.424397)
        self.CreatePin(title: "Old Poets Corner", subtitle: "A dog friendly pub", lat: 53.162818, long: -1.480225)
        self.CreatePin(title: "Hardwick Inn", subtitle: "A dog friendly pub", lat: 53.165225, long: -1.315326)
        self.CreatePin(title: "The Three Merry Lads", subtitle: "A dog friendly pub", lat: 53.257696, long: -1.483785)
        self.CreatePin(title: "The Peacock At Barlow", subtitle: "A dog friendly pub", lat: 53.267343, long: -1.484541)

        // sets the map so that it opens on the pin
//        let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 2000, longitudinalMeters: 2000)
//        mapView.setRegion(region, animated: true)
        
    }
    
    
    @IBAction func getDirectionsTapped(_ sender: Any) {
        getAddress()
    }
    
    // finds the place typed in and prints the longitude and latitude to the console
    func getAddress() {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(textFieldForAddress.text!) { (placemarks, error) in
            guard let placemarks = placemarks, let location = placemarks.first?.location
                else {
                    let alertController = UIAlertController(title: "Oops!",
                                                            message: "We couldn't find that location.", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default)
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion:
                        nil)
                    return
            }
            print(location)
            self.mapThis(destinationCord: location.coordinate)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
    }
    
    func mapThis(destinationCord: CLLocationCoordinate2D){
        let sourceCoordinate = (locationManager.location?.coordinate)!
        
        let sourcePlacemark = MKPlacemark(coordinate: sourceCoordinate)
        let destPlacemark = MKPlacemark(coordinate: destinationCord)
        
        let sourceItem = MKMapItem(placemark: sourcePlacemark)
        let destItem = MKMapItem(placemark: destPlacemark)
        
        let destinationRequest = MKDirections.Request()
        destinationRequest.source = sourceItem
        destinationRequest.destination = destItem
        destinationRequest.transportType = .automobile
        destinationRequest.requestsAlternateRoutes = true
        
        let directions = MKDirections(request: destinationRequest)
        directions.calculate { (response, error) in
            guard let response = response else {
                if let error = error {
                    print ("something is wrong", error)
                }
                return
            }
            
            let route = response.routes[0]
            self.mapView.addOverlay(route.polyline)
            self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
        }
     }
    
    func mapView(_ mapView: MKMapView, rendererFor Overlay: MKOverlay) -> MKOverlayRenderer {
        self.mapView.overlays.forEach {
            if ($0 is MKPolyline) {
                self.mapView.removeOverlay($0)
            }
        }
        
        let render = MKPolylineRenderer(overlay: Overlay as! MKPolyline)
        render.strokeColor = .blue
        return render
    }
    
    func CreatePin(title: String, subtitle: String, lat: Double, long: Double){
        // creates the pin
         let annotation = MKPointAnnotation()
        //sets the location of the pin
        annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        mapView.addAnnotation(annotation)

        // gives the pin a title and subtitle when clicked
        annotation.title = title
        annotation.subtitle = subtitle
    }
    
}
    
    
