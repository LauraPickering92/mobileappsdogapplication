//
//  GalleryViewController.swift
//  MobileAppsDogApplication
//
//  Created by Laura PIckering on 19/03/2020.
//  Copyright © 2020 Laura PIckering. All rights reserved.
//

import UIKit
import Photos

class GalleryViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet var imageSelected: UIImageView!
    
    @IBAction func openCamera(_ sender: Any) {
    // add code to access camera
       if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion:
                nil)
        }
    }
    
    @IBAction func openGallery(_ sender: Any) {
    // add code to access the Photo Gallery
    if
        UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion:
            nil)
    }
}
    
    @IBAction func savePhoto(_ sender: Any) {
    // add code to save photo
       let imageData = imageSelected.image!.jpegData(compressionQuality:
            0.6)
        let compressedJPGImage = UIImage(data: imageData!)
        UIImageWriteToSavedPhotosAlbum(compressedJPGImage!, nil,
                                       nil, nil)
        let alertController = UIAlertController(title: "Complete",
                                                message: "Your image has been saved.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion:
            nil)
    }
    
    // Define a function to select a photo
    func imagePickerController(_ picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey
    : Any]) {
     guard let image = info[.originalImage] as? UIImage else {
     fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
     }

     imageSelected.image = image
     dismiss(animated: true, completion: nil)
     }
}


extension PHPhotoLibrary {
    
    // MARK: - Public methods
    
    static func checkAuthorizationStatus(completion: @escaping (_ status: Bool) -> Void) {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            completion(true)
        } else {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if newStatus == PHAuthorizationStatus.authorized {
                    completion(true)
                } else {
                    completion(false)
                }
            })
        }
    }
    
    
}
