//
//  DogWalksViewController.swift
//  MobileAppsDogApplication
//
//  Created by Laura PIckering on 19/03/2020.
//  Copyright © 2020 Laura PIckering. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class DogWalksViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
        // managing the users location
        var locationManager = CLLocationManager()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            
            mapView.delegate = self
            
            self.CreatePin(title: "Peak District National Park", subtitle: "", lat: 53.317139, long: -1.429585)
            self.CreatePin(title: "Graves Park", subtitle: "", lat: 53.336088, long: -1.471116)
            self.CreatePin(title: "Queens Park", subtitle: "", lat: 53.233262, long: -1.433891)
            self.CreatePin(title: "Bluebell Woods", subtitle: "", lat: 53.445538, long: -1.980896)
            self.CreatePin(title: "Golf Course Dronfield", subtitle: "", lat: 53.292913, long: -1.461654)
            
            // sets the map so that it opens on the pin
    //        let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 2000, longitudinalMeters: 2000)
    //        mapView.setRegion(region, animated: true)
        }
        
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            print(locations)
        }
        
        func CreatePin(title: String, subtitle: String, lat: Double, long: Double){
            // creates the pin
             let annotation = MKPointAnnotation()
            //sets the location of the pin
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            mapView.addAnnotation(annotation)

            // gives the pin a title and subtitle when clicked
            annotation.title = title
            annotation.subtitle = subtitle
        }
        
    }
