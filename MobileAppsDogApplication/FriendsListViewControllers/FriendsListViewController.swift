//
//  FriendsListViewController.swift
//  MobileAppsDogApplication
//
//  Created by Laura PIckering on 19/03/2020.
//  Copyright © 2020 Laura PIckering. All rights reserved.
//

import UIKit

let names = ["Molly", "Penny", "Maisie", "Clementine", "Cassie", "Jack", "Percy", "Maggie"]

class FriendsListViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet var tableView: UITableView!
}

extension FriendsListViewController: UITableViewDelegate, UITableViewDataSource
{
    // return the Row Height
    func tableView(_ tableView: UITableView, heightForRowAt
    indexPath: IndexPath) -> CGFloat {
     return 120
     }
    
    // Return the number of items in the array
    func tableView(_ tableView: UITableView, numberOfRowsInSection
    section: Int) -> Int {
     return names.count
     }
    
    // Add text and Image to the cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath:
    IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? FriendsListTableViewCell
     cell?.friendsListLbl.text = names[indexPath.row]
     cell?.img.image = UIImage(named: names[indexPath.row])
     return cell!
     }
    
    // define function to load details of selected Contact
    func tableView(_ tableView: UITableView, didSelectRowAt
    indexPath: IndexPath) {
    let vc = storyboard?.instantiateViewController(withIdentifier:
    "FriendsDetailsViewController") as? FriendsDetailsViewController
     vc?.photo = UIImage(named: names[indexPath.row])!
     vc?.name = names[indexPath.row]
     self.navigationController?.pushViewController(vc!, animated: true)
     }
}
