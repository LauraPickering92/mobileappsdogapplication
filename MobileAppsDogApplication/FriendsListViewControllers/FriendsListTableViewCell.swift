//
//  FriendsListTableViewCell.swift
//  MobileAppsDogApplication
//
//  Created by Laura PIckering on 19/03/2020.
//  Copyright © 2020 Laura PIckering. All rights reserved.
//

import UIKit

class FriendsListTableViewCell: UITableViewCell {

    @IBOutlet var img: UIImageView!
    @IBOutlet var friendsListLbl: UILabel!
}
