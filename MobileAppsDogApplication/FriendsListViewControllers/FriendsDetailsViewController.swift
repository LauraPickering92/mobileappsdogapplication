//
//  FriendsDetailsViewController.swift
//  MobileAppsDogApplication
//
//  Created by Laura PIckering on 19/03/2020.
//  Copyright © 2020 Laura PIckering. All rights reserved.
//

import UIKit

class FriendsDetailsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // transfer the name and photo to the User Interface elements on the second View
        contactLbl.text="\(name)"
        img.image = photo
    }
    
    @IBOutlet var contactLbl: UILabel!
    @IBOutlet var img: UIImageView!
    
    // variables to store the name and image
    var photo = UIImage()
    var name = ""
}
