//
//  LoginViewController.swift
//  MobileAppsDogApplication
//
//  Created by Laura PIckering on 27/02/2020.
//  Copyright © 2020 Laura PIckering. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

class LoginViewController: UIViewController, UNUserNotificationCenterDelegate {

    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    
    // for notifications
    override func viewDidLoad() {
           super.viewDidLoad()
        
        registerCategories()
        
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        
        let content = UNMutableNotificationContent()
        content.title = "Reminder"
        content.body = "Did you check if you have any doggy to do's recently?"
        content.sound = .default
        
        content.categoryIdentifier = "alarm"
        content.userInfo = ["customData": "fizzbuzz"]
        
        var dateComponents = DateComponents()
        dateComponents.hour = 20
        dateComponents.minute = 04
        
        // this works with the calendar, so you can schedule when notifications get pushed through the device - this could be at the same time everyday
        //let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        // This notification works by pushing a notification through to the device after 10 seconds - this is good for demonstration purposes. These notifications also work when the device is locked.
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        
        let request = UNNotificationRequest(identifier: "testIdentifier", content: content,
                                            trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
       }
    
    // for notifications
    func registerCategories(){
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        
        let show = UNNotificationAction(identifier: "show", title: "Tell me more...", options: .foreground)
        
        let category = UNNotificationCategory(identifier: "alarm", actions: [show], intentIdentifiers: [])
        
        center.setNotificationCategories([category])
    }
    
    // for notifications
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        if let customData = userInfo["customData"] as? String{
            print ("Custom data recieved: \(customData)")
            
            switch response.actionIdentifier {
            case UNNotificationDefaultActionIdentifier:
                // the user swiped to unlock
                print("Default identifier")
            default:
                break
            }
        }
        
        completionHandler()
    }
    
    
    // for logging in
    @IBAction func AttemptLogin(_ sender: Any) {
               
        var result = [User]()
        
        if usernameField.text!.count == 0 || passwordField.text!.count == 0 {
            return
        }
        
        guard let manageContext = appDelegate?.persistentContainer.viewContext else {return}
               
        // Create an instance of NSFetchRequest which describes data you want retrieved
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.predicate = NSCompoundPredicate(type: .and, subpredicates: [NSPredicate(format: "username = %@", usernameField.text!), NSPredicate(format: "password = %@", passwordField.text!)])
        
        do {
            result = try manageContext.fetch(request) as! [User]
        }
        catch{
            print("Error while looking for user")
        }
        
        if result.count > 0 {
            print("found a user")
            let view = self.storyboard?.instantiateViewController(withIdentifier: "Homepage") as! HomepageViewController
            self.navigationController!.pushViewController(view, animated: true)
        }
        else{
            LoginValidation()
        }
        
    }
    
    // for validation & user interface
    func LoginValidation(){
            let alertController = UIAlertController(title: "Error",
                                                    message: "Could not find a user. Please try again.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:
                nil)
    }

}
