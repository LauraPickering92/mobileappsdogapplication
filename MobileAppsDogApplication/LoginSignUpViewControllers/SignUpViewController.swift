//
//  SignUpViewController.swift
//  MobileAppsDogApplication
//
//  Created by Laura PIckering on 27/02/2020.
//  Copyright © 2020 Laura PIckering. All rights reserved.
//

import UIKit
import CoreData

// let appDelegate = UIApplication.shared.delegate as? AppDelegate
var UserArray = [User]()

class SignUpViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func isEmpty(field: UITextField!) -> Bool{
        return field.text!.count == 0
    }
    
    func isFilled(field: UITextField!) -> Bool{
        return field.text!.count > 0
    }
    
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var confirmPasswordField: UITextField!
    
    // going to obtain data entered via the Text Fields and then transfer it to the entity we named User
    func saveForm(completion: (_ finished: Bool) -> ()){
        
        // Define instance of appDelegate
        guard let managedContext =
            appDelegate?.persistentContainer.viewContext else {return}
        
        // Define an instance of the entity UserSignUp
        let user = User(context: managedContext)
        
        // Transfer task details to the attributes
        user.email = emailField.text;
        user.username = usernameField.text;
        user.password = passwordField.text;
        
        // Save the task details, log the process and call the completion handler
        do {
            try managedContext.save()
            print("User Details Saved")
            completion(true)
        }
            // Use catch to capture the error
        catch {
            print("Failed to save data: ", error.localizedDescription)
            completion(false)
        }
        
    }
    
    @IBAction func submitSignUp(_ sender: Any) {
        
        if isEmpty(field: usernameField!) || isEmpty(field: passwordField) || isEmpty(field: emailField) || isEmpty(field: confirmPasswordField){
            let alertController = UIAlertController(title: "Error",
                                                    message: "One of your fields is empty. Please input information and try again", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:
                nil)
        }
            
            // Call the function saveForm()
            // Check if data has been saved – if the data has been saved navigate to main View Controller
            saveForm {(done) in
                if done{
                    navigationController?.popViewController(animated: true)
                    self.dismiss(animated: true, completion: nil)
                }
                    // If data is not saved log message “Data not saved. Try again”
                else {
                    print ("Data not saved. Try again ")
                }
        }
    }
}

extension SignUpViewController {
    // we define function retrieveData()
    func retrievehData(completion: (_ complete: Bool) -> ()){
        // Create an instance of appDelegate
        guard let manageContext =
            appDelegate?.persistentContainer.viewContext else {return}
        
        // Create an instance of NSFetchRequest which describes data you want retrieved
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        
        // Retrieve data from entity User, log the process and then call the completion handler
        do {
            UserArray = try manageContext.fetch(request) as! [User]
            print("Task details retrieved successfully")
            completion(true)
        }
            // If there is an error log the error and then call the completion handler
        catch {
            print("Unable to retrieve data: ", error)
            completion(false)
        }
        
    }
}

